import hashlib

async def login_parameters(login:str, password:str):
    hash_password = hashlib.sha256(password.encode()).hexdigest()
    return {"login":login, "password":hash_password}