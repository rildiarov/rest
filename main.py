import logging
import sqlite3
from fastapi import FastAPI, Depends, HTTPException, status
import database
import secrets
from parameters import token_parameters, login_parameters
from storage_tokens import Storage
import spech

app = FastAPI()
tokens = Storage()

# Создание объекта логгера
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Создание обработчика для записи логов в файл
file_handler = logging.FileHandler('mylog.log')
file_handler.setLevel(logging.INFO)

# Создание форматирования логов
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)

# Добавление обработчика к логгеру
logger.addHandler(file_handler)

@app.get("/salary/")
async def get_salary(token=Depends(token_parameters)):
    """ получение зарплаты по токену"""
    login = tokens.find_by_token(token['token'])
    if login is None:
        raise HTTPException(
            status_code=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION,
            detail="Try regenerate token or register to system"
        )
    salary = database.get_salary(conn, logger, "test_name")
    return {"salary": salary}


@app.get("/next_promotion/")
async def get_promotion(token=Depends(token_parameters)):
    """ Получение даты следующего повышения по токену """
    login = tokens.find_by_token(token['token'])
    if login is None:
        raise HTTPException(
            status_code=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION,
            detail="Try regenerate token or register to system"
        )
    date = database.get_next_promotion(conn, logger, login)
    return {"promotion_date": date}


def generate_token():
    key = secrets.token_urlsafe(30)
    return key


@app.get("/token/")
async def get_token(login_data=Depends(login_parameters)):
    """ генерация токена для юзера """
    if not database.exist_user(conn, logger, login_data['login']):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверные логин",
            headers={"WWW-Authenticate": "Basic"},
        )

    if not database.correct_password(conn, logger, login_data['login'], login_data['password']):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверные пароль",
            headers={"WWW-Authenticate": "Password"},
        )

    token = generate_token()
    tokens.add(login_data['login'], token)

    return {"token": token}


if __name__ == '__main__':
    # подключение к базе данных
    conn = sqlite3.connect(spech.db_path)

    database.create_table(conn, logger)
    database.generate_data(conn, logger)

    # запуск логирования
    logging.basicConfig(level=logging.INFO)
