class Storage:
    def __init__(self):
        self.login_token = dict()

    def add(self, login, token):
        self.login_token[login] = token

    def find_by_token(self, token):
        for login in self.login_token:
            if self.login_token[login] == token:
                self.login_token.pop(login)
                return login
        return None
