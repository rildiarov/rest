import dataclasses
import datetime

@dataclasses.dataclass
class User:
    login:str
    password:str
    salary:int
    next_promotion:datetime