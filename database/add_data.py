import hashlib
from models import User


def add_new_user(conn, logger, user: User):
    try:
        c = conn.cursor()
        c.execute(f"INSERT INTO employee (login, password, salary, next_promotion_date) VALUES (?, ?, ?, ?)",
                  (user.login, user.password, user.salary, user.next_promotion, ))
        conn.commit()
        logger.info(f"Новый пользователь успешно добавлен в базу данных: {User.login}")
    except Exception as e:
        logger.error(f"Ошибка при добавлении нового пользователя: {e}")


def generate_data(conn, logger):
    try:
        password = hashlib.sha256("12345".encode()).hexdigest()
        user = User("test_name", password, 68_500, "11.10.2023")

        add_new_user(conn, user)
    except Exception as e:
        logger.error(f"Ошибка при генерации данных: {e}")
