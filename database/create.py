def create_table(conn, logger):
    try:
        c = conn.cursor()

        c.execute('''CREATE TABLE IF NOT EXISTS employee
                     (id INTEGER PRIMARY KEY AUTOINCREMENT,
                     login TEXT(256) UNIQUE,
                     password TEXT,
                     salary INT,
                     next_promotion_date DATE)''')
        conn.commit()

        logger.info("Таблица 'employee' успешно создана или уже существует")
    except Exception as e:
        logger.error(f"Ошибка при создании таблицы 'employee': {e}")
