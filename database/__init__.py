from .get_data import exist_user, get_salary, get_next_promotion, correct_password
from .create import create_table
from .add_data import generate_data
