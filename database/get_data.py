def exist_user(conn, logger, login):
    c = conn.cursor()
    c.execute('SELECT id FROM employee WHERE login=?', (login,))
    index = c.fetchone()

    res = bool(index)
    logger.info(f"Проверка существования пользователя: {login}({res})")

    return res


def get_salary(conn, logger, login):
    salary = -1
    try:
        c = conn.cursor()
        c.execute('SELECT salary FROM employee WHERE login=?', (login,))
        salary = c.fetchone()
    except Exception as e:
        logger.error(f"Ошибка получения зарплаты пользователя: {e}")
    else:
        logger.info(f"Получение зарплаты пользователя: {login}")

    return salary[0]


def get_next_promotion(conn, logger, login):
    next_promotion = -1
    try:
        c = conn.cursor()
        c.execute(f'SELECT next_promotion_date FROM employee WHERE login=?', (login,))
        next_promotion = c.fetchone()
    except Exception as e:
        logger.error(f"Ошибка получения даты повышения пользователя : {e}")
    else:
        logger.info(f"Получение даты следующего повышения пользователя: {login}")

    return next_promotion[0]


def correct_password(conn, logger, login, password):
    c = conn.cursor()
    c.execute(f'SELECT password FROM employee WHERE login=?', (login,))
    password_db = c.fetchone()

    res = password == password_db[0]

    logger.info(f"Проверка пароля пользователя: {login}({res})")

    return

